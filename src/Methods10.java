public class Methods10 {
	public static void main(String[] args) {
		printQuestion(2);
		question2();

		printQuestion(4);
		question4();

		printQuestion(7);
		question7();

		printQuestion(8);
		question8();

		printQuestion(10);
		question10();
	}

	public static void question2() {
		first();
		second();
	}

	public static void first() {
		System.out.println("Hello!");
		second();
	}

	public static void second() {
		System.out.println("Hi!");
	}

	public static void question4() {
		for (int i = 1; i < 10; i++) {
			if (i % 7 == 0) {
				output(i);
			}
		}

	}

	public static void output(int c) {
		System.out.println(c * 2);
	}

	public static void question7() {
		int a = 7;
		doSomething(a);
		System.out.println(a);
	}

	public static void doSomething(int a) {
		a = a + a++;
	}

	public static void question8() {
		int sum = 0;
		for (int i = 0; i < 4; i++) {
			sum = sum + calculate(i, i + 1);
			if (i > 2) {
				sum = sum * calculate(sum, sum + 1);
			}
		}

		System.out.println(calledCalculate);

	}

	public static int calledCalculate;

	public static int calculate(int a, int b) {
		calledCalculate++;
		return 0;
	}

	public static void question10() {
		String a = "abcd";
		String b = "efghi";
		output(a + b, b + "jkl");
	}

	public static void output(String a, String b) {
		if (a.length() > b.length()) {
			System.out.println(a.length());
		} else {
			System.out.println(b.length());
		}
	}

	// helper method
	public static void printQuestion(int question) {
		System.out.println("================");
		System.out.println("Question " + question);
	}

}

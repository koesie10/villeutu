import java.util.Random;

public class Selection9 {

	public static void output(String binary) {
		// This is the easiest you can get
		// System.out.println(Integer.parseInt(binary, 2));

		// Difficult way they want you to do it
		double j = 0;
		for (int i = 0; i < binary.length(); i++) {
			if (binary.charAt(i) == '1') {
				j = j + Math.pow(2, binary.length() - 1 - i);
			}
		}
		System.out.println(j);
	}

	public static void main(String[] args) {
		final Random r = new Random();
		for (int i = 0; i < 4; i++) {
			String bin = "";
			for (int j = 0; j < 4; j++) {
				bin += r.nextInt(2);
			}
			System.out.print("Binary string " + bin + " is in decimal: ");
			output(bin);
		}
	}

}

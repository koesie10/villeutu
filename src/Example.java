public class Example {
	private int a;
	private double b;

	public Example(int a, int b) {
		this.a = a;
		this.b = b;
	}

	public Example() {
		a = 4;
		b = 2.0;
	}

	public void setValues(int a, double b) {
		this.a = a;
		this.b = b;
	}

	public void calculate() {
		b = a / b;
		a++;
	}

	// Helper methods
	public int getA() {
		return a;
	}

	public double getB() {
		return b;
	}

}

public class Classes7 {
	public static void main(String[] args) {
		printQuestion(1);
		question1();

		printQuestion(2);
		question2();
		
		printQuestion(4);
		question4();
		
		printQuestion(6);
		question6();
		
		printQuestion(9);
		question9();
	}

	public static void question1() {
		Example myExample = new Example(1, 2);
		
		System.out.println("a == " + myExample.getA() + ", b == "
				+ myExample.getB());
	}

	public static void question2() {
		Example test = new Example();
		test.setValues(2, 4.0);
		test.calculate();
		
		System.out.println(test.getB());
	}
	
	public static void question4() {
		Example test = new Example();
		for (int i=0; i < 3; i++){
			test.calculate();
		}
		
		System.out.println(test.getA());
	}
	
	public static void question6() {
		Example ob2 = new Example(1, 4);
		Example ob1 = new Example();
		ob1.calculate();
		
		System.out.println(ob2.getB());
	}
	
	public static void question9() {
		Example firstObject = new Example(2,3);
		Example secondObject = firstObject;
		secondObject.setValues(4, 4);
		firstObject.calculate();
		
		System.out.println(firstObject.getA());
	}

	// helper method
	public static void printQuestion(int question) {
		System.out.println("================");
		System.out.println("Question " + question);
	}

}

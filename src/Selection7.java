import java.util.Random;

public class Selection7 {

	public static void output(String word) {
		if (word.length() % 2 == 0) {
			System.out.println("none");
		} else {
			System.out.println(word.charAt(word.length() / 2));
		}
	}

	public static void main(String[] args) {
		final String[] words = { "rare", "letters", "feta", "appreciates",
				"programming", "programmer", "key", "paragraph", "automate",
				"student", "lame", "compare", "evaluate", "designing",
				"development", "call", "define", "dynamic", "snap", "place" };
		final Random r = new Random();

		for (int i = 0; i < 10; i++) {
			final String w = words[r.nextInt(words.length)];
			System.out.print("The word is : " + w);
			System.out.print(" and the middle letter is: ");
			output(w);
		}

	}

}

import java.util.Random;

public class Methods3 {

	public static void main(String[] args) {
		Random r = new Random();

		int n = r.nextInt(10) + 10;

		for (int i = 0; i < n; i++) {
			if (r.nextInt(2) == 1) {
				Methods3.printHello();
			} else {
				Methods3.printWorld();
			}
		}
	}

	public static void printHello() {
		System.out.println("Hello");
	}

	public static void printWorld() {
		System.out.println("World");
	}

}

import java.util.Random;

public class Classes3 {
	private int x;

	public Classes3() {
	}

	public static void main(String[] args) {
		Classes3 t = new Classes3();
		Random r = new Random();

		for (int i = 0; i < 10; i++) {
			t.setX(r.nextInt(10));
			System.out.println(t.getX());
		}
	}
	
	public void setX( int newX) {
		x = newX;
	}
	
	public int getX() {
		return x;
	}

}

import java.util.Random;

public class Classes5 {
	public static void main(String[] args) {
		Random r = new Random();
		//r.setSeed(Long.parseLong(args[0])); removed

		int x = r.nextInt(50);
		int y = r.nextInt(50);
		int direction = r.nextInt(4) + 1;

		Ship ship = new Ship(x, y, direction);

		System.out.println("X: " + ship.getX() + ", Y: " + ship.getY()
				+ ", Direction: " + ship.getDirection());

		int units = r.nextInt(10) + 1;
		direction = r.nextInt(4) + 1;

		ship.setDirection(direction);
		ship.move(units);

		System.out.println("X: " + ship.getX() + ", Y: " + ship.getY()
				+ ", Direction: " + ship.getDirection());

		direction = r.nextInt(4) + 1;
		units = r.nextInt(10) + 1;

		ship.setDirection(direction);
		ship.move(units);

		System.out.println("X: " + ship.getX() + ", Y: " + ship.getY()
				+ ", Direction: " + ship.getDirection());
	}
}

class Ship {
	private int direction;
	private int x;
	private int y;
	
	public Ship(int x, int y, int direction) {
		this.x = x;
		this.y = y;
		this.direction = direction;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getDirection() {
		return direction;
	}
	
	public void setDirection(int direction) {
		this.direction = direction;
	}
	
	public void move(int units) {
		if (direction == 1) {
			y += units;
		} else if (direction == 2) {
			x += units;
		} else if (direction == 3) {
			y -= units;
		} else if (direction == 4) {
			x -= units;
		}
	}
}

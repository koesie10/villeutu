import java.util.Random;

public class Strings4 {

	public static void output(String word) {
		System.out.println(word.charAt(0));
		System.out.println(word.charAt(word.length() / 2));
		System.out.println(word.charAt(word.length() - 1));
	}

	public static void main(String[] args) {
		final String[] words = { "quick", "letters", "apple", "appreciates",
				"programming", "zippers", "key", "paragraph", "admin",
				"student", "teacher", "compare", "dog", "designing",
				"development", "studies", "run", "dynamic", "fox", "place" };
		final Random r = new Random();

		for (int i = 0; i < 4; i++) {
			final String w = words[r.nextInt(words.length)];
			System.out.println("The word is : " + w);
			System.out.println("1st, middle and last letters are:");
			output(w);
		}

	}

}

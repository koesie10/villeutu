import java.util.Random;

public class Methods7 {
	public static void main(String[] args) {
		Random r = new Random();
		// r.setSeed(Long.parseLong(args[0])); No input in this example, just a
		// random seed

		for (int i = 0; i < 5; i++) {
			int n1 = r.nextInt(5) + 5;
			int n2 = r.nextInt(10) + 20;

			System.out.println("Sum of odd numbers between " + n1 + " and "
					+ n2 + " is " + Methods7.sumOfOdd(n1, n2));
		}
	}

	public static int sumOfOdd(int low, int high) {
		int sum = 0;
		for (int i = low; i <= high; i++) {
			if (i % 2 == 1) {
				sum += i;
			}
		}
		return sum;
	}
}

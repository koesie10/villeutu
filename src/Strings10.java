public class Strings10 {

	public static void main(String[] args) {
		// question 5
		String s = "result:" + 1 + 2 + (1 + 2);
		int len = s.length();
		System.out.println(len);

		// question 8
		String str = "abcdefgh";
		int a = str.length() - 1;
		String myStr = "" + str.charAt(a--) + str.charAt(a--) + str.charAt(a--);
		System.out.println(myStr);

		// question 10
		String string = "Quick brown fox";
		System.out.println(string.substring(6).substring(6));

	}

}

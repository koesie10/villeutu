// 7. Writing classes
// 2. Quiz: Classes and objects 1
public class Person {
	private String name;
	private int age;
	
	public Person() {
		//assigning default values
		
		//default value for the name
		name = "nobody";
		
		//default value for the age
		age = 99;
	}
	
	public void printDetails() {
		System.out.println(name + ":" + age);
	}
}

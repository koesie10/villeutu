import java.util.Random;

public class Repetition9 {
	public static void main(String[] args) {
		Random r = new Random();

		int n1 = r.nextInt(10);
		int n2 = r.nextInt(30) + 15;

		System.out.println("Values of n1, n2: " + n1 + " " + n2);
		for (int i = n1; i < n2; i++) {
			if (i % 2 == 0 && i % 3 != 0) {
				System.out.println(i);
			}
		}
	}

}

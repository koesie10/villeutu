public class Introduction1 {
	public static void main(String[] args) {
		System.out.println("G'day!");
		// Example of a comment line.
		// Declaring and assigning variables
		int c = 3;
		c = c + 4;
		int d = 2;
		// assigning a value to variable after declaration
		d = 7; // Displaying a variable to the user
		System.out.println("The value of d is : " + d);
		System.out.println("This is easy! Forward one step!");
	}
}
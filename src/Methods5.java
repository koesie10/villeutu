public class Methods5 {
	public static void main(String[] args) {
		System.out.println("Calculating audio-file size, CD-quality");
		int x = 3;
		wavMusicToMP3OneMinute();
		wavMusicToMP3FourMinute();
	}

	public static void wavMusicToMP3OneMinute() {
		int amount = 10;
		System.out.println(amount + " megs in mp3: " + amount / 10);
	}

	public static void wavMusicToMP3FourMinute() {
		int size = 40;
		System.out.println(size + " megs in mp3: " + size / 10);
	}
}

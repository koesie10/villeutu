import java.util.Random;

public class DataTypes3 {
	public static void main(String[] args) {
		Random r = new Random();

		int n1 = r.nextInt(90) + 10;
		int n2 = r.nextInt(100) + 50;
		int n3 = r.nextInt(100) + 100;
		System.out.println("Number1:" + n1);
		System.out.println("Number2:" + n2);
		System.out.println("Number3:" + n3);
	}
}
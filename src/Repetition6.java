public class Repetition6 {
	public static void main(String[] args) {
		int counter = 1;
		boolean condition = true;
		while (condition) {
			counter = counter * 2;
			if (counter >= 7) {
				condition = false;
			}
			System.out.println(counter);
		}
	}

}

public class Selection10 {

	public static void main(String[] args) {
		// Question 1
		int a = 3;
		int b = 5;
		if (a++ > b && a++ == 4) {
			b++;
		}
		System.out.println(a);

		// Question 2
		int r1 = 11;
		int r2 = r1++;
		System.out.println(r1 > r2);

		// Question 4
		String s1 = "Hello";
		String s2 = s1.substring(0, s1.length() - 1);
		boolean truth = s1.length() == s2.length();
		System.out.println(truth);

		// Question 5
		boolean tru;
		if (a > b) {
			tru = false;
		} else {
			tru = true;
		}
		System.out.println(tru);

		// Question 6
		int n = 4;
		int m = 6;

		if (n > m) {
			n++;
		} else if (n == m) {
			n--;
		} else {
			n = m++;
		}
		System.out.println(n);

		// Question 7
		int n1 = 5;
		int n2 = 3;
		int n3 = 6;
		if (n1 > n2 && n2 < n3 && n1 != n3) {
			System.out.println("executed");
		}

		// Question 8 (commented out becauseit throws an exception
		// int a8 = 3;
		// int b8 = 3;
		// int c8 = 3;
		// if (a8 == b8 == c8){
		// System.out.println(a8 + b8 + c8);
		// } else {
		// System.out.println(a8 + b8);
		// }

		// Question 9
		boolean b1 = true;
		boolean b2 = !b1;
		System.out.println(b1 || b2 && (b1 && !b2));

		// Question 10
		question10();

	}

	public static void question10() {
		int a = 3;
		boolean b1 = a % 2 == 0; // false
		boolean b2 = !(b1 || false); // true
		boolean b3 = !(b1 && b2); // true

		System.out.println("Question 10: ");
		System.out.println(b1 || b2 && b3);
		System.out.println(b1 || !b2 || !b3);
		System.out.println(b1 && b2 && b3);
		System.out.println(b1 && b2 || b3);
		System.out.println(b1 || !(b2 && b3));
	}

}

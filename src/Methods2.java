public class Methods2 {
	public static void main(String[] args) {
		int x = 7;
		System.out.println("Timestable for " + x);
		timesTable();
		dashLine();
	}

	public static void timesTable() {
		int y = 7;
		dashLine();
		for (int i = 1; i < 7; i = i + 2) {
			System.out.print(y + "*" + i + " is ");
			System.out.println(i * y);
		}
		dashLine();
	}

	public static void dashLine() {
		System.out.println("--------");
	}
}

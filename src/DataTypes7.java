public class DataTypes7 {
	public static void main(String[] args) {
		int a = 0;
		int b = a++;
		int c = b++ + a++;
		System.out.println(a - b);
		System.out.println(a + b);
		a = a + b++;
		System.out.println(a++);
		System.out.println(a + ++b);
	}
}
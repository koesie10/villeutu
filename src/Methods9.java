import java.util.Random;

public class Methods9 {
	public static void main(String[] args) {
		Random r = new Random();
		// r.setSeed(Long.parseLong(args[0])); No input in this example, just a
		// random seed
		String chars = "abcdeiforusy";
		int len = r.nextInt(10) + 10;
		String str = "";
		for (int j = 0; j < len; j++) {
			str += chars.charAt(r.nextInt(chars.length()));
		}
		System.out.println("The string is: " + str);
		Methods9.outputVowels(str);
	}

	public static void outputVowels(String str) {
		for (int i = 0; i < str.length(); i++) {
			switch (str.charAt(i)) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
			case 'y':
				System.out.println(str.charAt(i));
				break;
			default:
				break;
			}
		}
	}
}

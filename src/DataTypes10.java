public class DataTypes10 {
	public static void main(String[] args) {
		question7();
		question8();
	}

	public static void question7() {
		// Question 7
		int a = 2;
		int b = 7;
		int c = ++a;
		int d = ++a + c-- + ++b;
		System.out.println(d);
	}

	public static void question8() {
		// Question 8
		int a = 5;
		int b = 2;
		int c = a / b;
		double d = a / b;
		System.out.println("c == " + c + ", d == " + d);
	}
}
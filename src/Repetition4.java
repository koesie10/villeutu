import java.util.Random;

public class Repetition4 {

	public static void outputFactorial(int n) {
		int value = 1;
		for (int i = 1; i <= n; i++) {
			value *= i;
		}
		System.out.println(value);
	}

	public static void main(String[] args) {
		Random r = new Random();

		for (int i = 0; i < 5; i++) {
			int number = r.nextInt(10) + 1;
			System.out.print("Factorial of " + number + " is: ");
			outputFactorial(number);
		}
	}

}

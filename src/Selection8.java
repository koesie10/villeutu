public class Selection8 {
	public static void main(String[] args) {
		String s = "To be a Java programmer";
		int a = 4;
		int b = 6;
		if (b++ > a) {
			b = b + 1;
			if (b < a++) {
				b--;
				a = a - 1;
			} else {
				a = a + b--;
			}
		} else {
			b = b + a++;

		}
		System.out.println(s.substring(b, b + a));
	}
}

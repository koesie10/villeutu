import java.util.Random;

public class Selection4 {

	public static void output(int n1, int n2, int n3) {
		// Efficient solution
		if (n1 > n2) {
			if (n2 > n3) {
				System.out.println(n2);
			} else if (n1 > n3) {
				System.out.println(n3);
			} else {
				System.out.println(n1);
			}
		} else {
			if (n1 > n3) {
				System.out.println(n1);
			} else if (n2 > n3) {
				System.out.println(n3);
			} else {
				System.out.println(n2);
			}
		}
	}

	public static void main(String[] args) {
		Random r = new Random();
		int n1, n2, n3;

		for (int i = 0; i < 4; i++) {
			n1 = r.nextInt(90) + 10;
			n2 = r.nextInt(90) + 10;
			n3 = r.nextInt(90) + 10;
			System.out.print("Values of numbers: " + n1 + " " + n2 + " " + n3
					+ ", middle: ");
			output(n1, n2, n3);
		}

	}

}

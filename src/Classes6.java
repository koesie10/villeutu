import java.util.Random;

public class Classes6 {
	public static void main(String[] args) {
		Random r = new Random();

		Rectangle rectangle = new Rectangle(r.nextInt(10) + 1,
				r.nextInt(10) + 1);
		rectangle.setLocation(new Point(r.nextInt(10), r.nextInt(10)));
		Rectangle rectangle2 = new Rectangle(r.nextInt(10) + 1,
				r.nextInt(10) + 1, new Point(r.nextInt(10), r.nextInt(10)));

		printRectangleInfo(rectangle);
		printRectangleInfo(rectangle2);

		rectangle.setWidth(r.nextInt(10) + 5);
		rectangle.setHeight(r.nextInt(10) + 5);
		rectangle.setLocation(r.nextInt(10), r.nextInt(10));

		printRectangleInfo(rectangle);

		rectangle2.getLocation().setX(r.nextInt(10));
		rectangle2.getLocation().setY(r.nextInt(10));
		rectangle2.setWidth(r.nextInt(10) + 1);
		rectangle2.setHeight(r.nextInt(10) + 1);

		printRectangleInfo(rectangle2);

		for (int i = 0; i < 10; i++) {
			Point p = new Point(r.nextInt(10), r.nextInt(10));
			if (rectangle.isInsideArea(p)) {
				System.out.println("Inside area!");
			} else {
				System.out.println("Not inside area!");
			}
		}
	}

	public static void printRectangleInfo(Rectangle rectangle) {
		System.out.println(rectangle.getWidth());
		System.out.println(rectangle.getHeight());
		System.out.println(rectangle.getArea());
		System.out.println(rectangle.getLocation().getX());
		System.out.println(rectangle.getLocation().getY());
	}
}

class Rectangle {
	private int width;
	private int height;
	private Point location;

	public Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
		this.location = new Point(0, 0);
		
		// Simpler:
		// this(width, height, new Point(0, 0);
	}

	public Rectangle(int width, int height, Point location) {
		this.width = width;
		this.height = height;
		this.location = location;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public Point getLocation() {
		return location;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public void setLocation(int x, int y) {
		this.location = new Point(x, y);
	}

	public int getArea() {
		return width * height;
	}

	public boolean isInsideArea(Point point) {
		// Stolen from OpenJDK java.awt.Rectangle#inside(int X, int Y)
		int X = point.getX();
		int Y = point.getY();

		int w = this.width;
		int h = this.height;
		if ((w | h) < 0) {
			// At least one of the dimensions is negative...
			return false;
		}
		// Note: if either dimension is zero, tests below must return false...
		int x = this.location.getX();
		int y = this.location.getY();
		if (X < x || Y < y) {
			return false;
		}
		w += x;
		h += y;
		// overflow || intersect
		return ((w < x || w > X) && (h < y || h > Y));
	}

}

class Point {
	private int x;
	private int y;

	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

}
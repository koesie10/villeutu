public class Repetition10 {
	public static void main(String[] args) {
		printQuestion(1);
		question1();

		printQuestion(2);
		question2();

		printQuestion(3);
		question3();

		printQuestion(4);
		question4();

		printQuestion(8);
		question8();

		printQuestion(9);
		question9();

		printQuestion(10);
		question10();
	}

	public static void question1() {
		int a = 3;
		for (int i = 9; i > a; i--) {
			System.out.println(i * i);
			a++;
		}
	}

	public static void question2() {
		String s = "Hello";
		int ind = 0;
		while (s.length() > 0) {
			if (s.length() == 2) {
				continue;
			} else if (s.charAt(s.length() - 1) == 'l') {
				break;
			}
			s = s.substring(0, s.length() - 1);
			ind++;
		}

		// Call println to check for value of variable
		System.out.println(ind);
	}

	public static void question3() {
		int r = 5;
		for (int i = 0; i < r; i++) {
			System.out.println(i);
			r--;
		}
	}

	public static void question4() {
		String s = "abcdef";
		while (s.length() > 0) {
			System.out.print(s.charAt(s.length() - 1));
			s = s.substring(0, s.length() - 2);
		}
	}

	public static void question8() {
		int f = 11;
		int g = f / 2;
		do {
			g++;
			f--;
		} while (g < 5);
		System.out.println(f * g);
	}

	public static void question9() {
		// own code to check all values until 20
		for (int x = 0; x < 20; x++) {
			System.out.println("-------------");
			System.out.println("x = " + x);

			// real code
			int a = x;
			while (a-- > 10) {
				System.out.println(a / 2);
				a -= 2;
			}
			// end real code
		}
	}

	public static void question10() {
		System.out.println("---------------");
		System.out.println("Fragment 1");
		System.out.println("---------------");
		question10_fragment1();
		System.out.println("---------------");
		System.out.println("Fragment 2");
		System.out.println("---------------");
		question10_fragment2();
	}

	public static void question10_fragment1() {
		int a = 10;
		do {
			System.out.println(a--);
		} while (a % 10 != 0);
	}

	public static void question10_fragment2() {
		int a = 10;
		while (a % 10 != 0) {
			System.out.println(a--);
		}
	}

	// helper method
	public static void printQuestion(int question) {
		System.out.println();
		System.out.println("================");
		System.out.println("Question " + question);
	}
}

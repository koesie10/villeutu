import java.util.Random;

public class DataTypes9 {
	public static void main(String[] args) {
		Random r = new Random();

		int n1 = r.nextInt(90) + 10;
		int n2 = r.nextInt(100) + 50;
		int n3 = r.nextInt(100) + 100;

		System.out.println(n1 + n2 + n3);
		System.out.println((n1 + n2 + n3) / 3.0);
	}
}
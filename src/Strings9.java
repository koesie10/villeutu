import java.util.Random;

public class Strings9 {

	public static void outputAge(String nameBirth) {
		// Compact version (commented out as the line is probably a bit too
		// long)
		// System.out.println(2011 -
		// Integer.parseInt(nameBirth.substring(nameBirth.length()-4,
		// nameBirth.length())));

		// Extended version
		String birth = nameBirth.substring(nameBirth.length() - 4,
				nameBirth.length());
		int birthYear = Integer.parseInt(birth);
		int age = 2011 - birthYear;
		System.out.println(age);
	}

	public static void main(String[] args) {
		final String[] first = { "John", "James", "Bill", "Arnold", "Lisa",
				"Ann", "Kimberly", "Monica" };
		final String[] last = { "Smith", "Jones", "Williams", "Brown",
				"Wilson", "Taylor", "Johnson" };
		Random r = new Random();

		for (int i = 0; i < 3; i++) {

			String name = first[r.nextInt(first.length)] + " "
					+ last[r.nextInt(last.length)];
			name += ", 19" + r.nextInt(9) + r.nextInt(10);

			System.out.print("Name and birth year:" + name + ". ");
			System.out.print("Age: ");
			outputAge(name);
		}
	}
}

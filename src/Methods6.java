public class Methods6 {
	public static void main(String[] args) {
		System.out.println(foo(1, 2, 3));
		System.out.println(bar(4, 5, 1));
		System.out.println(foo(bar(8, 4, 5), foo(4, 9, 12), bar(5, 3, 2)));
	}

	public static int foo(int a, int b, int c) {
		int d = a + b + c;
		return d / 3;
	}

	public static int bar(int a, int b, int c) {
		int d = a - b - c;
		return d * 3;
	}

}

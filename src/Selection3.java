public class Selection3 {
	public static void main(String[] args) {
		double number = 125;
		int min = 100;
		int max = 150;
		if (number > min && number < max) {
			number = number + 25;
			System.out.println(number);
		}
		if (number <= min || number >= max) {
			number = number / 2;
			System.out.println(number);
		} else {
			number = number * 2;
			System.out.println(number);
		}
		if (number < 100 && number % 5 == 0) {
			number = number / 5;
			System.out.println(number);
		}
	}

}
